import React, { Suspense } from 'react';

import DatGui, { DatBoolean, DatColor, DatNumber} from 'react-dat-gui';

import Workspace from './components/Editor/Workspace';
import Filepicker from './components/Editor/Filepicker';
import LoadedModel from './components/Editor/LoadedModel';
import PropertiesEditor from './components/Editor/PropertiesEditor';
import ModelTree from './components/Editor/ModelTree';

import Logo from './img/mr3v-logo.png';

import './style.css';
import 'semantic-ui-less/semantic.less';

import * as THREE from 'three';

export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      currentModel: null,
      hoveredModel: null,
      selectedModels: [],
      deletedModels: [],
      data: {
        color: '#FF5733',
        scale: 1,
        opacity: 1,
      },
    };
  }

  handleFileChange = (loadedModel) => {
    this.setState({ currentModel: loadedModel });
  }

  handleColorChange = newCol => {
    this.setState(prevState=>({data: {opacity: prevState.data.opacity, scale: prevState.data.scale, color: newCol.hex}}));
    if (this.state.selectedModels.length > 0) {
      console.log('changing of selected');
      this.state.selectedModels.forEach(m => {
        if (m instanceof THREE.Mesh) {
          var newMaterial = m.material.clone();
          newMaterial.color = new THREE.Color(newCol.hex);
          m.material = newMaterial;
        }
        if (m.children) {
          m.children.forEach(mChild => {
            if (mChild instanceof THREE.Mesh) {
              var newMaterial = mChild.material.clone();
              newMaterial.color = new THREE.Color(newCol.hex);
              mChild.material = newMaterial;
            }
          });
        }
      });
    }
    else {
      this.state.currentModel.scene.children.forEach(child => {
        child.children.forEach(obj => {
          if (obj instanceof THREE.Mesh) {
            obj.material.color = new THREE.Color(newCol.hex);
          }
        });
      });
    }
  }

  handleUpdate = newData => {
    this.setState(prevState => ({
      data: {opacity: newData.opacity, scale: newData.scale},
    }));
    
    if (this.state.currentModel) {
      if (this.state.selectedModels.length > 0) {
        console.log('changing of selected');
        console.log(this.state.selectedModels);
        this.state.selectedModels.forEach(m => {
          m.scale.set(newData.scale, newData.scale, newData.scale);
          if (m instanceof THREE.Mesh) {
            var newMaterial = m.material.clone();
            if (this.state.data.opacity < 1) {
              newMaterial.side = THREE.DoubleSide;
              newMaterial.opacity = newData.opacity;
              newMaterial.transparent = true;
            }
            else {
              newMaterial.transparent = false;
            }
            m.material = newMaterial;
          }
          if (m.children) {
            m.children.forEach(mChild => {
              if (mChild instanceof THREE.Mesh) {
                mChild.scale.set(newData.scale, newData.scale, newData.scale);
                var newMaterial = mChild.material.clone();
                if (this.state.data.opacity < 1) {
                  newMaterial.side = THREE.DoubleSide;
                  newMaterial.opacity = newData.opacity;
                  newMaterial.transparent = true;
                }
                else {
                  newMaterial.transparent = false;
                }
                mChild.material = newMaterial;
              }
            });
          }
        });
      }

      else {
        this.state.currentModel.scene.children.forEach(child => {
          child.scale.set(newData.scale, newData.scale, newData.scale);
        });
        if (this.state.data.opacity < 1) {
          this.state.currentModel.scene.children.forEach(child => {
            child.children.forEach(obj => {
              if (obj instanceof THREE.Mesh) {
                obj.material.side = THREE.DoubleSide;
                obj.material.opacity = newData.opacity;
                obj.material.transparent = true;
              }
            });
          });
        }
        else {
          this.state.currentModel.scene.children.forEach(child => {
            child.children.forEach(obj => {
              if (obj instanceof THREE.Mesh) {
                obj.material.transparent = false;
              }
            });
          });
        }
      }
    }
  }

  handleModelTreeElementMouseOver = model => {
    this.setState({ hoveredModel: model });
  }

  handleModelTreeElementMouseOut = model => {
    this.setState({ hoveredModel: null });
  }

  handleModelTreeElementSelect = model => {
    this.setState({ selectedModels: [model] });
  }

  handleModelTreeElementUnSelect = model => {
    this.setState({ selectedModels: [] });
  }

  handleModelTreeElementDelete = model => {
    this.state.deletedModels.push(model);
    this.deleteModel(model);
  }

  deleteModel = model => {
    this.state.deletedModels.forEach(m => {
      if (m instanceof THREE.Mesh) {
        m.geometry.dispose();
        m.material.dispose();
      }
      if (m.parent) {
        m.parent.remove(m);
      }
      else {
        this.state.currentModel.scene.remove(m);
      }
      console.log('deleted');
    });
  }

  render() {
    return (
      <div className="ui-container">
        <div className="ui-controls">
          <div className="brand">
            <img src={Logo} alt="mr3v logo" />
            <h1>Mixed reality 3D Viewer</h1>
          </div>
          <div className="sidebar-container">
            <div className="filepicker-sidebar">
              <Filepicker
                onFileChange={this.handleFileChange}
              />
            </div>
          </div>
          <div className="model-tree-container">
            <ModelTree
              model={this.state.currentModel}
              onElementMouseOver={this.handleModelTreeElementMouseOver}
              onElementMouseOut={this.handleModelTreeElementMouseOut}
              onElementSelect={this.handleModelTreeElementSelect}
              onElementUnselect={this.handleModelTreeElementUnSelect}
              onElementDelete={this.handleModelTreeElementDelete}
            />
          </div>
        </div>

        <div className="editor-workspace">
          <div className="react-dat-gui-relative-position">
          <PropertiesEditor
            data={this.state.data}
            onUpdate={this.handleUpdate}
            onColChange={this.handleColorChange}
          />
          </div>
          <Workspace>
            <LoadedModel
              data={this.state.data}
              loadedModel={this.state.currentModel}
              selectedModels={this.state.selectedModels}
              hoveredModel={this.state.hoveredModel}
            />
          </Workspace>
          
        </div>

      </div>
    );
  }
}
