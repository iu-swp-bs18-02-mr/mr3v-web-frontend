import React from 'react';

import { ax } from 'tools';

import { Input, Button, Grid, Card, Feed } from 'semantic-ui-react';
import FileList from './FilesList';

import './style.css';

export default class Filepicker extends React.Component {
  fileInputRef = React.createRef();
  fileReader = new FileReader();

  constructor(props) {
    super(props);

    this.state = {
      files: [],
      searchText: "",
    };
    ax.get(`/getFileList`)
      .then(respose =>
        this.setState({
          files: respose.data.filenames,
        })
      )
      .catch(error => {
        alert('An error occurred when saving the model, check the console for details');
        console.log(error);
      });
  }

  handleUploadFile = e => {
    Array.from(e.target.files).forEach(file => {
      this.fileReader.onloadend = (completedFileRead) => {
        ax.post(
          `/saveModel`,
          completedFileRead.target.result,
          {
            headers: { 'Filename': file.name },
          }
        )
        .then(async response => {
          const fileListResponse = await ax.get(`/getFileList`);
          this.setState({
            files: fileListResponse.data.filenames,
          });
        })
        .catch(error => {
          alert('An error occurred when saving the model, check the console for details');
          console.log(error);
        });
      };
      this.fileReader.readAsArrayBuffer(file);
    });
  };

  render() {
    return (
      <Grid padded className="full-height">
      <Grid.Column className="full-height">
      <Card fluid className="full-height">
        <Card.Content className="filepicker-search-input">
          <Card.Header className="filepicker-search-input">
            <Input
              fluid
              size="small"
              icon="search"
              iconPosition="left"
              placeholder="Search files..."
              onChange={(e, data) => this.setState({ searchText: data.value })}
            />
          </Card.Header>
        </Card.Content>
        <Card.Content className="scrollable">
          <Feed>
            <FileList
              files={
                this.state.files
                  .filter((filename) =>
                   filename.search(this.state.searchText) !== -1
                  )
              }
              onFileChange={this.props.onFileChange}
            />
          </Feed>
        </Card.Content>
        <Card.Content extra className="no-min-height">
          <Button
            fluid
            primary
            size="medium"
            onClick={() => this.fileInputRef.current.click()}
          >
            Upload new file
          </Button>
          <input
            ref={this.fileInputRef}
            type="file"
            hidden
            onChange={this.handleUploadFile}
          />
        </Card.Content>
      </Card>
      </Grid.Column>
      </Grid>
    );
  }
}
