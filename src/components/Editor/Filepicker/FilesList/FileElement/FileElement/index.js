import React from 'react';

import { ax } from 'tools';

import { Card } from 'semantic-ui-react';

export default class FileElement extends React.Component {
  handleClick = (loader, fileName) => {
    ax.get(
      `/getModel/${fileName}`,
      {
        responseType: 'arraybuffer',
      }
    )
      .then(response =>
        loader.parse(
          response.data,
          '',
          (loadedModel) => {
            this.props.onFileChange(loadedModel);
          },
          (error) => {
            alert('Error loading the model, check the console for details');
            console.log(error);
          }
        )
      )
      .catch(error => {
        alert('An error occurred, check the console for details');
        console.log(error);
      });
  }

  render() {
    const fileName = this.props.fileName;
    const getRestProps = ({fileName, fileContent, onFileChange, ...rest}) => rest;

    return (
      <Card
        {...getRestProps(this.props)}
        fluid
        link
        className="file-entry-container"
        onClick={() => this.handleClick(this.props.loader, fileName)}
      >
        <Card.Content>
          <Card.Description>
            <b>{ fileName }</b>
          </Card.Description>
        </Card.Content>
      </Card>
    );
  }
}
