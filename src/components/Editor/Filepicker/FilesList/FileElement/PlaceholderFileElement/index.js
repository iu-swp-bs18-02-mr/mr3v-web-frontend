import React from 'react';

import { Placeholder, Card } from 'semantic-ui-react';

import './style.css';

export default class FileElement extends React.Component {
  render() {
    return (
      <Card fluid className="file-entry-container">
        <Card.Content>
          <Placeholder inverted className="file-entry-placeholder">
            <Placeholder.Header image inverted>
              <Placeholder.Line length="full" inverted/>
              <Placeholder.Line length="medium" inverted/>
            </Placeholder.Header>
          </Placeholder>
        </Card.Content>
      </Card>
    );
  }
}
