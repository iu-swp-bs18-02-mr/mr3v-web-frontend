import React from 'react';

import { Card } from 'semantic-ui-react';

import { useLoader } from 'react-three-fiber';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';

import { FileElement, PlaceholderFileElement } from './FileElement';

import './style.css';

export default class FileList extends React.Component {
  gltfLoader = new GLTFLoader();

  render() {
    // TODO: fetch files from server
    return (
      <Card.Group className="filepicker-files">
        {
          (this.props.files && this.props.files.map((file, i) => {
            return (
              <FileElement
                key={i}
                fileName={file}
                loader={this.gltfLoader}
                onFileChange={this.props.onFileChange}
              />
            );
          }))
        }
      </Card.Group>
    );
  }
}
