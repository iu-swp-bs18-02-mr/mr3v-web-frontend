import React from 'react';
import { useRef } from 'react';

import { Canvas, useFrame, useThree, extend } from 'react-three-fiber';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
extend({ OrbitControls });

function CameraControls() {
  const ref = useRef();
  const { camera, gl } = useThree();
  useFrame(() => ref.current.update());
  return <orbitControls ref={ref} args={[camera, gl.domElement]} enableDamping dampingFactor={0.1} rotateSpeed={0.5} />;
}

export default class Workspace extends React.Component {
  render() {
    return (
      <Canvas
        style={{ background: window.getComputedStyle(document.documentElement).getPropertyValue('--workspace-background-color') }}
      >
        <directionalLight
          color={0xFFFFFF}
          intensity={0.5}
        />
        <ambientLight />
        <CameraControls />
        {this.props.children}
      </Canvas>
    );
  }
}
