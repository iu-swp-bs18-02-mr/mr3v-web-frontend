import React from 'react';

import './style.css';

import DelIcon from './delete.png';

export default class TreeNode extends React.Component {
  isSelected = () =>
    this.props.selectedElementId === this.props.model.uuid

  render() {
    if (this.props.model)
    {
      return (<>
        <div
          className={
            `tree-node ${
              this.isSelected() ?
              "selected-tree-node" :
              ""}`
          }
          onMouseOver={_ => this.props.onElementMouseOver(this.props.model)}
          onMouseOut={_ => this.props.onElementMouseOut(this.props.model)}
          onClick={_ =>
            this.isSelected() ?
            this.props.onElementUnselect(this.props.model) :
            this.props.onElementSelect(this.props.model)
          }
        >
          {
            new Array(this.props.depth)
            .fill(<span className="tree-node-indent" />)
          }
          <span className="node-content">
            {this.props.model.name || this.props.model.type} 
            
          </span>
          <img src={DelIcon} alt='delete icon' margon='auto' float='right' width='25px' height='25px' onClick={_ =>
            this.props.onElementDelete(this.props.model)
          }></img>
         
        </div>
        {this.props.model.children.map(child => {
          return (
            <TreeNode
              key={child.uuid}
              selectedElementId={this.props.selectedElementId}
              depth={this.props.depth + 1}
              model={child}
              onElementMouseOver={this.props.onElementMouseOver}
              onElementMouseOut={this.props.onElementMouseOut}
              onElementSelect={this.props.onElementSelect}
              onElementUnselect={this.props.onElementUnselect}
              onElementDelete={this.props.onElementDelete}
            />
          );
        })}
      </>);
    }

    return <></>;
  }
}
