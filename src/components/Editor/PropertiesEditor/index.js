import React, { Suspense } from 'react';
import DatGui, { DatBoolean, DatColor, DatNumber, DatFolder} from 'react-dat-gui';
import { SketchPicker } from 'react-color';

export default class PropertiesEditor extends React.Component {
  handleColorChange = ({ hex }) => console.log(hex);

  render() {
    return (
      <DatGui data={this.props.data} onUpdate={this.props.onUpdate} className="react-dat-gui">
      <DatNumber path='scale' label='Scale' min={0} max={10} step={0.1} />
      <DatNumber path='opacity' label='Opacity' min={0} max={1} step={0.1} />
      <SketchPicker label='Color' color={this.props.data.color} onChangeComplete={this.props.onColChange} />
      </DatGui>
    );
  }
}
